"""
Abstracts a GUI window
"""
from Tkinter import *
class Window(Tk):
    def __init__(self):
        self.initgui()
    def initgui():
        """
        Defined in subclasses
        """
        pass

