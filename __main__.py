"""
Launch the main gui when running this as a package
"""
from __future__ import absolute_import
from .gui import main

main()
