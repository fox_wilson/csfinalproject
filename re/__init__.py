"""
This package serves as a font-end to the python re module
"""
from __future__ import absolute_import
from .gui import main
_gui = True
_gui_name = "Regular Expressions"
